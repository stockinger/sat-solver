SAT Solver Basics
==============
:toc: macro 

Author:   Stefan Stockinger

Email:     stefan.stockinger@technikum-wien.at

Date:      17.02.2017

toc::[]

== PicoSAT Tutorial

PicoSAT is a SAT solver for boolean expressions. Here you will find a brief tutorial.

=== Setup

First of all download latest the PicoSAT version from 
http://fmv.jku.at/picosat/[here^].
Unzip the files and follow the steps in the README.

=== Easy example

After the installation you can start PicoSAT in your Terminal. As input file PicoSAR requires a *DIMACS CNF* format file, which can be passed as parameter. 

Just a very brief example file:


[source,python]
----
 c expression:(x1 || !x2) && (x3)
 p cnf 3 2
 1 -2 0
 3 0
----
 
The letter *c* indicates a line comment, which means the line with "c expression:(x1 || !x2) && x3" is a comment.

In the next line *p cnf 3 2* decribes, that the SAT problem is formatted in cnf format. 3 means, that there are three variables in the expression. 2 indicates that the expression consists of 2 clauses.

the line *1 -2 0* describes the first clause *(x1 || !x2)*. If a variable is true, the value is positive, if false, the value is negativ. Furthermore, variable 1 can have the value 1 or -1, the second 2 or -2, the third 3 or -3 and so on... 

If a variable is not present in a clause, you don't have to write any value for it (as variable x3 is not present in the first clause, there is no 3 or -3 written in the third line of this example file). Do not forget that every line must end with * 0*.

The last line *3 0* represents the second clause (x3).
Save the file - i have choosen `veryeasy.dimacs` as file name and saved the file into the subfolder `files`.


Before we run PicoSAT let us take a look at the expression and think about the result:
----
(x1 || !x2) && (x3)
----

x3 has always to be true to make the whole expression true. Additionally, wheter x1 has to be true or x2 has to be false.

Possible values to make the whole expression true:

-----
x1 = true, x2 = false, x3 = true
x1 = false, x2 = false, x3 = true
x1 = true, x2 = true, x3 = true
-----

Now let us run PicoSAT, if we get the same solution...

To start PicoSAT with this file just open the terminal and change the directory to the picosat folder using the `cd` command and type in the following command:


----
 $ ./picosat --all ./files/veryeasy.dimacs 
----
Here is the output:

----
s SATISFIABLE
v 1 -2 3 0               <- x1 = true, x2 = false, x3 = true
s SATISFIABLE
v 1 2 3 0                <- x1 = true, x2 = true, x3 = true
s SATISFIABLE
v -1 -2 3 0              <- x1 = false, x2 = false, x3 = true
s SOLUTIONS 3
----

PicoSAT found 3 solutions. The same as we expected. Which means PicoSAT, as well as our brain works. ;)

Now let's adapt the example to make the expression unsatisfiable, just for demonstration:

[source,python]
----
c expression: (x1 || !x2) && x3 && !x3
p cnf 3 3
1 -2 -3 0
3 0
-3 0
----
A third clause was added with "!x3". Now the expression can never be true, because x3 can not be true and false at the same time. 


If we now run PicoSAT with this file, we get the following output:

----
s SOLUTIONS 0
----

*SOLUTIONS 0* means, that there does not exist any value combination of the variables for this expression to make it true. Furthermore, this expression will always be false.

=== Another example

Here is another, even more complex example:

[source,python]
----
c (x1 || !x2) && (x1 || x3) && (x3 || !x4 || x5 || !x6 || !x7) && (x8 || x9) && x10
p cnf 10 5
1 -2 0
1 3 0
3 -4 5 -6 -7 0
8 9 0
10 0
----

For this example PicoSAT delivers 234 Solutions, which i will not post here for non-spaming reasons.

== Definitions


==== Model checking

Model checking is the verification of mathematical system description (model) against a specification. The process is fully automated since it does not require any user interaction. The aim of the algorithm is to find a violation of the specification. It also outputs a counterexample, if there is a violation found.

==== Theorem Proving

Theorem proving is a machine-assisted proof procedure. Using computer programs, logical theorems are mathematically proven. There are interactive and fully automated theorem provers available.


==== SAT Solver
SAT Solver is the short form of `Satisfiability Solver`. A SAT Solver checks a boolean expression if it can be satisfied, or in other words, if it there is at least one combination of the input variables which makes the expression true. PicoSAT, which was used for the tutorial above is one of these SAT solvers. PicoSAT also delivers all Solutions.
 
==== SMT Solver
SMT Solver is the short form of `Satisfiability Modulo Theory Solver`.
A SMT Solver expands the SAT solver with variable data types. SAT Solver only support boolean variables. For an SMT solver, different data types (int, float) can be used.
An SMT solver deals with the decision problem of logical formulas
